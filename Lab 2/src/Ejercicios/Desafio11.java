package Ejercicios;

import java.util.Random;

public class Desafio11 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		CuentaCorriente cta1 = new CuentaCorriente("Carlitos", 2000);
		CuentaCorriente cta2 = new CuentaCorriente("Anita", 3000);
		
		CuentaCorriente.Transferencia(cta1, cta2, 500);
		
		System.out.println(cta1.toString());
		System.out.println(cta2.toString());

	}

}

class CuentaCorriente {

	private double saldo;
	private String nombreTitular;
	private long numeroCuenta;
	public CuentaCorriente(String nomString, double saldo) {
		this.nombreTitular = nomString;
		this.saldo = saldo;

		Random aleatorio = new Random();
		this.numeroCuenta = Math.abs(aleatorio.nextLong());
	}
	public CuentaCorriente(String nomString) {
		this.nombreTitular = nomString;
		this.saldo = 0;
		Random aleatorio = new Random();
		this.numeroCuenta = Math.abs(aleatorio.nextLong());
	}
	public String ingresarDiner(double dinero) {
		if(dinero >0) {
			this.saldo += dinero;
			return "Se ha ingresado $"+ dinero;
		}else {
			return "no se puede ingresar un valor negativo";
		}
	}

	public void sacarDinero(double dinero) {
		this.saldo -= dinero;
	}
	
	public static void Transferencia(CuentaCorriente ctaSalida, CuentaCorriente ctaEntra, double dinero) {
		ctaEntra.saldo += dinero;
		ctaSalida.saldo -= dinero;
	}
	@Override
	public String toString() {
		return "\n nombreTitular = " + nombreTitular+
				"\n saldo = " + saldo + 
				"\n numeroCuenta = " + numeroCuenta + "\n";
	}
	
}
