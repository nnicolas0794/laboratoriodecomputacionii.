package DesafioIndividual6;

import java.awt.Color;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Main {

	private JFrame frame;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Main () {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize () {
		frame = new JFrame();
		frame.setBounds(100, 100, 400, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.print(" Se ah producido un evento ");
			}
		});
		panel.setBackground(Color.WHITE);
		panel.setBounds(0, 0, 386, 363);
		frame.getContentPane().add(panel);
		
		JButton btnviolet = new JButton("Violet");
		btnviolet.setHorizontalAlignment(SwingConstants.LEFT);
		panel.add(btnviolet);
		btnviolet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel.setBackground(new Color(138, 43, 226));
				System.out.print(" El elemento Violeta tiene el foco ");
			}
		});
		btnviolet.setFont(new Font("Calibri", Font.PLAIN, 20));
		JButton btngray = new JButton("Gray");
		panel.add(btngray);
		btngray.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel.setBackground(Color.gray);
				System.out.print(" El elemento Gray tiene el foco ");
			}
		});
		btngray.setFont(new Font("Calibri", Font.PLAIN, 20));
	}
}