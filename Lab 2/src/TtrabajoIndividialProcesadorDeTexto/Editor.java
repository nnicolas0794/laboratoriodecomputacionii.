package TtrabajoIndividialProcesadorDeTexto;

import java.awt.EventQueue;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JMenu;
import javax.swing.text.StyledEditorKit;
import java.io.File;
import javax.swing.JMenuBar;
import java.awt.Font;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.swing.JScrollPane;
public class Editor implements ActionListener {
	private JFrame frmNewEditorText;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Editor window = new Editor();
					window.frmNewEditorText.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Editor() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	JMenuItem mntmNewMenuItem_5;
	JMenuItem mntmNewMenuItem_6;
	JTextPane textPane;
	JMenuItem mntmNewMenuItem_12;
	private void initialize() {
		frmNewEditorText = new JFrame();
		frmNewEditorText.setTitle("New Editor Text");
		frmNewEditorText.setBackground(Color.WHITE);
		frmNewEditorText.getContentPane().setBackground(Color.WHITE);
		frmNewEditorText.setBounds(100, 100, 500, 400);
		frmNewEditorText.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmNewEditorText.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(5, 10, 520, 420);
		
		textPane = new JTextPane();
		scrollPane.setViewportView(textPane);
		textPane.setFont(new Font("Arial", Font.PLAIN, 11));
		frmNewEditorText.getContentPane().add(scrollPane, BorderLayout.CENTER);
		textPane.setPreferredSize(new Dimension (450,350));
		
		JMenuBar menuBar = new JMenuBar();
		frmNewEditorText.getContentPane().add(menuBar, BorderLayout.NORTH);
		
		JMenu mnNewMenu = new JMenu("Archivos");
		mnNewMenu.setFont(new Font("Arial", Font.PLAIN, 12));
		menuBar.add(mnNewMenu);
		
		mntmNewMenuItem_12 = new JMenuItem("Nuevo ");
		mntmNewMenuItem_12.addActionListener(this);
		mntmNewMenuItem_12.setFont(new Font("Arial", Font.PLAIN, 12));
		mnNewMenu.add(mntmNewMenuItem_12);
		
		mntmNewMenuItem_5 = new JMenuItem("Abrir");
		mntmNewMenuItem_5.addActionListener(this);
		mntmNewMenuItem_5.setFont(new Font("Arial", Font.PLAIN, 12));
		mnNewMenu.add(mntmNewMenuItem_5);
		
		
		mntmNewMenuItem_6 = new JMenuItem("Guardar");
		mntmNewMenuItem_6.addActionListener(this);
		mntmNewMenuItem_6.setFont(new Font("Arial", Font.PLAIN, 12));
		mnNewMenu.add(mntmNewMenuItem_6);
		
		JMenu mnNewMenu_1 = new JMenu("Fuente");
		mnNewMenu_1.setFont(new Font("Arial", Font.PLAIN, 12));
		menuBar.add(mnNewMenu_1);
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Arial");
		mntmNewMenuItem_1.setFont(new Font("Arial", Font.PLAIN, 12));
		mnNewMenu_1.add(mntmNewMenuItem_1);
		mntmNewMenuItem_1.addActionListener(new StyledEditorKit.FontFamilyAction("", "Arial"));
		
		JMenuItem mntmNewMenuItem_2 = new JMenuItem("Calibri");
		mntmNewMenuItem_2.setFont(new Font("Arial", Font.PLAIN, 12));
		mnNewMenu_1.add(mntmNewMenuItem_2);
		mntmNewMenuItem_2.addActionListener(new StyledEditorKit.FontFamilyAction("","Calibri"));
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Tahoma");
		mntmNewMenuItem.setFont(new Font("Arial", Font.PLAIN, 12));
		mnNewMenu_1.add(mntmNewMenuItem);
		mntmNewMenuItem.addActionListener(new StyledEditorKit.FontFamilyAction("","Tahoma"));
		
		JMenu mnNewMenu_2 = new JMenu("Estilo");
		mnNewMenu_2.setFont(new Font("Arial", Font.PLAIN, 12));
		menuBar.add(mnNewMenu_2);
		
		JMenuItem mntmNewMenuItem_3 = new JMenuItem("Cursiva");
		mntmNewMenuItem_3.setFont(new Font("Arial", Font.PLAIN, 12));
		mnNewMenu_2.add(mntmNewMenuItem_3);
		mntmNewMenuItem_3.addActionListener(new StyledEditorKit.ItalicAction());
		mntmNewMenuItem_3.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_K, InputEvent.CTRL_DOWN_MASK));
		
		JMenuItem mntmNewMenuItem_4 = new JMenuItem("Negrita");
		mntmNewMenuItem_4.setFont(new Font("Arial", Font.PLAIN, 12));
		mnNewMenu_2.add(mntmNewMenuItem_4);
		mntmNewMenuItem_4.addActionListener(new StyledEditorKit.BoldAction());
		mntmNewMenuItem_4.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK));
		
		JMenu mnNewMenu_3 = new JMenu("Tamanio");
		mnNewMenu_3.setFont(new Font("Arial", Font.PLAIN, 12));
		menuBar.add(mnNewMenu_3);
		
		JMenuItem mntmNewMenuItem_7 = new JMenuItem("11");
		mntmNewMenuItem_7.setFont(new Font("Arial", Font.PLAIN, 12));
		mnNewMenu_3.add(mntmNewMenuItem_7);
		mntmNewMenuItem_7.addActionListener(new StyledEditorKit.FontSizeAction("",11));
		
		JMenuItem mntmNewMenuItem_8 = new JMenuItem("12");
		mntmNewMenuItem_8.setFont(new Font("Arial", Font.PLAIN, 12));
		mnNewMenu_3.add(mntmNewMenuItem_8);
		mntmNewMenuItem_8.addActionListener(new StyledEditorKit.FontSizeAction("",12));
		
		JMenuItem mntmNewMenuItem_9 = new JMenuItem("14");
		mntmNewMenuItem_9.setFont(new Font("Arial", Font.PLAIN, 12));
		mnNewMenu_3.add(mntmNewMenuItem_9);
		mntmNewMenuItem_9.addActionListener(new StyledEditorKit.FontSizeAction("",14));
		
		JMenuItem mntmNewMenuItem_10 = new JMenuItem("17");
		mntmNewMenuItem_10.setFont(new Font("Arial", Font.PLAIN, 12));
		mnNewMenu_3.add(mntmNewMenuItem_10);
		mntmNewMenuItem_10.addActionListener(new StyledEditorKit.FontSizeAction("",17));
		
		JMenuItem mntmNewMenuItem_11 = new JMenuItem("20");
		mntmNewMenuItem_11.setFont(new Font("Arial", Font.PLAIN, 12));
		mnNewMenu_3.add(mntmNewMenuItem_11);
		mntmNewMenuItem_11.addActionListener(new StyledEditorKit.FontSizeAction("",20));
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource()== mntmNewMenuItem_5) {
			abrir();
		}else if (e.getSource()== mntmNewMenuItem_6 ) {
			guardar();
		}else if (e.getSource() == mntmNewMenuItem_12 ) {
			nuevo();
		}
	}
	File url = null;
	public void abrir() {
		String arch = "";

        JFileChooser fchooser = new JFileChooser();

        int seleccion = fchooser.showOpenDialog(null);
        if (seleccion == JFileChooser.APPROVE_OPTION) {

            url = fchooser.getSelectedFile();

            try {
                FileInputStream entrada = new FileInputStream(url);

                int caracter;

                while ((caracter = entrada.read()) != -1) {
                    char letra = (char) caracter;
                    arch += letra;
                }
                entrada.close();

            } catch (IOException e) {
                JOptionPane.showMessageDialog(null, "Ha ocurrido un Error");
                System.out.println(e);
            }
        }
	textPane.setText(arch);	
	}
	public void guardar() {
		String arch = textPane.getText();

        if (url == null) {
            JFileChooser fchooserhooser = new JFileChooser();

            int seleccion = fchooserhooser.showSaveDialog(null);
            if (seleccion == JFileChooser.APPROVE_OPTION) {

                url = fchooserhooser.getSelectedFile();
                try {
                    FileOutputStream salida = new FileOutputStream(url);

                    for (int i = 0; i < arch.length(); i++) {
                        salida.write(arch.charAt(i));
                    }
                    salida.close();

                } catch (IOException e) {
                    JOptionPane.showMessageDialog(null, "Ha ocurrido un Error");
                    System.out.println(e);
                }
            }
        } else {
            try {
                FileOutputStream salida = new FileOutputStream(url);

                for (int i = 0; i < arch.length(); i++) {
                    salida.write(arch.charAt(i));
                }
                salida.close();

            } catch (IOException e) {
                JOptionPane.showMessageDialog(null, "Ha ocurrido un Error");
                System.out.println(e);
            }
        }
	}
	public void nuevo () {
        String arch = textPane.getText();

        if (!arch.equals("")) {
            int seleccion = JOptionPane.showOptionDialog(null, "Quiere guardar los cambios?", "Atencion!",
                    JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null,
                    new Object[] { "Guardar", "No guardar", "Cancelar" }, "opcion 1");
            if (seleccion != -1) {
                if (seleccion == 0) {
                    guardar();
                } else if (seleccion == 1) {
                    textPane.setText("");
                }
            }

        }

	}
}