package TrabajoPracticoLaCalculadora;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Main {

	private JFrame frame;
	private JTextField textField;
	
	double pnum;
	double snum;
	double result;
	String operacion;
	String respuesta;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Main() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(135, 206, 235));
		frame.setBounds(100, 100, 400, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textField = new JTextField();
		textField.setHorizontalAlignment(SwingConstants.RIGHT);
		textField.setFont(new Font("Calibri", Font.PLAIN, 35));
		textField.setBackground(new Color(255, 255, 255));
		textField.setBounds(22, 22, 342, 90);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btn7 = new JButton("7");
		btn7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String enternumber = textField.getText() + btn7.getText();
				textField.setText(enternumber);
			}
		});
		btn7.setBackground(new Color(240, 248, 255));
		btn7.setFont(new Font("Tahoma", Font.BOLD, 25));
		btn7.setBounds(22, 143, 70, 70);
		frame.getContentPane().add(btn7);
		
		JButton btn8 = new JButton("8");
		btn8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String enternumber = textField.getText() + btn8.getText();
				textField.setText(enternumber);
			}
		});
		btn8.setBackground(new Color(240, 248, 255));
		btn8.setFont(new Font("Tahoma", Font.BOLD, 25));
		btn8.setBounds(102, 143, 70, 70);
		frame.getContentPane().add(btn8);
		
		JButton btn9 = new JButton("9");
		btn9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String enternumber = textField.getText() + btn9.getText();
				textField.setText(enternumber);
			}
		});
		btn9.setBackground(new Color(240, 248, 255));
		btn9.setFont(new Font("Tahoma", Font.BOLD, 25));
		btn9.setBounds(182, 143, 70, 70);
		frame.getContentPane().add(btn9);
		
		JButton btn4 = new JButton("4");
		btn4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String enternumber = textField.getText() + btn4.getText();
				textField.setText(enternumber);
			}
		});
		btn4.setBackground(new Color(240, 248, 255));
		btn4.setFont(new Font("Tahoma", Font.BOLD, 25));
		btn4.setBounds(22, 226, 70, 70);
		frame.getContentPane().add(btn4);
		
		JButton btn5 = new JButton("5");
		btn5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String enternumber = textField.getText() + btn5.getText();
				textField.setText(enternumber);
			}
		});
		btn5.setBackground(new Color(240, 248, 255));
		btn5.setFont(new Font("Tahoma", Font.BOLD, 25));
		btn5.setBounds(102, 223, 70, 70);
		frame.getContentPane().add(btn5);
		
		JButton btn6 = new JButton("6");
		btn6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String enternumber = textField.getText() + btn6.getText();
				textField.setText(enternumber);
			}
		});
		btn6.setBackground(new Color(240, 248, 255));
		btn6.setFont(new Font("Tahoma", Font.BOLD, 25));
		btn6.setBounds(182, 223, 70, 70);
		frame.getContentPane().add(btn6);
		
		JButton btn1 = new JButton("1");
		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String enternumber = textField.getText() + btn1.getText();
				textField.setText(enternumber);
			}
		});
		btn1.setBackground(new Color(240, 248, 255));
		btn1.setFont(new Font("Tahoma", Font.BOLD, 25));
		btn1.setBounds(22, 306, 70, 70);
		frame.getContentPane().add(btn1);
		
		JButton btn2 = new JButton("2");
		btn2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String enternumber = textField.getText() + btn2.getText();
				textField.setText(enternumber);
			}
		});
		btn2.setBackground(new Color(240, 248, 255));
		btn2.setFont(new Font("Tahoma", Font.BOLD, 25));
		btn2.setBounds(102, 303, 70, 70);
		frame.getContentPane().add(btn2);
		
		JButton btn3 = new JButton("3");
		btn3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String enternumber = textField.getText() + btn3.getText();
				textField.setText(enternumber);
			}
		});
		btn3.setBackground(new Color(240, 248, 255));
		btn3.setFont(new Font("Tahoma", Font.BOLD, 25));
		btn3.setBounds(182, 303, 70, 70);
		frame.getContentPane().add(btn3);
		
		JButton btnnew = new JButton("C");
		btnnew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField.setText(null);
			}
		});
		btnnew.setBackground(new Color(240, 248, 255));
		btnnew.setFont(new Font("Tahoma", Font.BOLD, 25));
		btnnew.setBounds(275, 143, 70, 70);
		frame.getContentPane().add(btnnew);
		
		JButton btnsub = new JButton("-");
		btnsub.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pnum = Double.parseDouble(textField.getText());
				textField.setText("");
				operacion = "-";
			}
		});
		btnsub.setBackground(new Color(240, 248, 255));
		btnsub.setFont(new Font("Tahoma", Font.BOLD, 25));
		btnsub.setBounds(275, 306, 70, 70);
		frame.getContentPane().add(btnsub);
		
		JButton btnplus = new JButton("+");
		btnplus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pnum = Double.parseDouble(textField.getText());
				textField.setText("");
				operacion = "+";
			}
		});
		btnplus.setBackground(new Color(240, 248, 255));
		btnplus.setFont(new Font("Tahoma", Font.BOLD, 25));
		btnplus.setBounds(275, 226, 70, 70);
		frame.getContentPane().add(btnplus);
		
		JButton btn0 = new JButton("0");
		btn0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String enternumber = textField.getText() + btn0.getText();
				textField.setText(enternumber);
			}
		});
		btn0.setBackground(new Color(240, 248, 255));
		btn0.setFont(new Font("Tahoma", Font.BOLD, 25));
		btn0.setBounds(22, 383, 230, 70);
		frame.getContentPane().add(btn0);
		
		JButton btnequal = new JButton("=");
		btnequal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				snum = Double.parseDouble(textField.getText());
				if (operacion == "+") {
					result = pnum + snum ;
					respuesta = String.format("%.2f", result);
					textField.setText(respuesta);
				}
				if (operacion == "-") {
					result = pnum - snum ;
					respuesta = String.format("%.2f", result);
					textField.setText(respuesta);
				}
				if (operacion == "*") {
					result = pnum * snum ;
					respuesta = String.format("%.2f", result);
					textField.setText(respuesta);
				}
				if (operacion == "/") {
					result = pnum / snum ;
					respuesta = String.format("%.2f", result);
					textField.setText(respuesta);
				}
			}
		});
		btnequal.setForeground(new Color(0, 0, 0));
		btnequal.setBackground(new Color(240, 248, 255));
		btnequal.setFont(new Font("Tahoma", Font.BOLD, 25));
		btnequal.setBounds(182, 463, 163, 70);
		frame.getContentPane().add(btnequal);
		
		JButton btndiv = new JButton("/");
		btndiv.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pnum = Double.parseDouble(textField.getText());
				textField.setText("");
				operacion = "/";
			}
		});
		btndiv.setBackground(new Color(240, 248, 255));
		btndiv.setFont(new Font("Tahoma", Font.BOLD, 25));
		btndiv.setBounds(22, 463, 70, 70);
		frame.getContentPane().add(btndiv);
		
		JButton btnmult = new JButton("*");
		btnmult.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pnum = Double.parseDouble(textField.getText());
				textField.setText("");
				operacion = "*";
			}
		});
		btnmult.setFont(new Font("Tahoma", Font.BOLD, 25));
		btnmult.setBackground(new Color(240, 248, 255));
		btnmult.setBounds(275, 383, 70, 70);
		frame.getContentPane().add(btnmult);
		
		JButton btnpunt = new JButton(".");
		btnpunt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(! textField.getText().contains("."))
		          {
					textField.setText(textField.getText() + btnpunt.getText());
		          }
			}
		});
		btnpunt.setFont(new Font("Tahoma", Font.BOLD, 25));
		btnpunt.setBackground(new Color(240, 248, 255));
		btnpunt.setBounds(102, 463, 70, 70);
		frame.getContentPane().add(btnpunt);
	}
}
