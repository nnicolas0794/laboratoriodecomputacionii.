package TrabajoIndividialBanco;

import java.io.Serializable;

public class CuentaCorriente implements Serializable {
	private static final long serialVersionUID = 1L;

private String nombre;
private double saldo;
private int numero;
public CuentaCorriente(String nombre, double saldo, int numero) {
	this.nombre = nombre;
	this.saldo = saldo;
	this.numero = numero;
}
public String getNombre() {
	return nombre;
}
public void setNombre(String nombre) {
	this.nombre = nombre;
}
public double getSaldo() {
	return saldo;
}
public void setSaldo(double saldo) {
	this.saldo = saldo;
}
public int getNumero() {
	return numero;
}
public void setNumero(int numero) {
	this.numero = numero;
}
@Override
public String toString() {
	return "\n Nombre del titular = " + nombre + "\n Saldo inicial = " + saldo + "\n N� de cuenta corriente = " + numero + "\n";
	}
public void sacar(double monto) {
	this.saldo=saldo-monto;
	
}
public void ingresar(double monto) {
	this.saldo=saldo+monto;
	
}
}
