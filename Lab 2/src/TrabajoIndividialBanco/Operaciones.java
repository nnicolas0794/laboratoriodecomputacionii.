package TrabajoIndividialBanco;

public class Operaciones {
	public static void Transferir (CuentaCorriente cuentaOrigen, CuentaCorriente cuentaDestino, double monto ) {
		cuentaOrigen.sacar(monto);
        cuentaDestino.ingresar(monto);
	}
	public static void Depositar(CuentaCorriente cuentaOrigen, double imp1) {
		cuentaOrigen.ingresar(imp1);
	}
	public static void Extraer (CuentaCorriente cuentaOrigen, double imp2) {
		cuentaOrigen.sacar(imp2);
  }
}
